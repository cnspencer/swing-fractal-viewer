
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class FractalPopupMenu extends JPopupMenu {
	private JMenuItem juliaFractalOption;
	private FractalPanel fractalPanel;
	private double positionX;
	private double positionY;
	
	public FractalPopupMenu (FractalPanel fp, double x, double y) {
		fractalPanel = fp;
		positionX = x;
		positionY = y;
		
		juliaFractalOption = new JMenuItem("Julia Fractal");
		add(juliaFractalOption);
		
		juliaFractalOption.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				FractalViewer fv = new FractalViewer(FractalPanel.FractalType.JULIA_SET, positionX, positionY);
				fv.setDefaultCloseOperation(FractalViewer.DISPOSE_ON_CLOSE);
			}
		});
	}
}

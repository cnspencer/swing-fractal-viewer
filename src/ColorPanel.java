//package main_package;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.Semaphore;


public class ColorPanel extends JPanel implements AdjustmentListener, Runnable{
	private JPanel labelPanel = new JPanel();	
	private JLabel redLabel = new JLabel("Red");
	private JLabel greenLabel = new JLabel("Green");
	private JLabel blueLabel = new JLabel("Blue");
	private JLabel gradientLabel = new JLabel("Gradient");
	
	private JPanel scrollPanel = new JPanel();
	private JScrollBar redBar;
	private JScrollBar greenBar;
	private JScrollBar blueBar;
	private JScrollBar gradientBar;
	
	private FractalPanel fractalPanel;
	
	private long lastAdjustTime = System.currentTimeMillis();
	private boolean adjust = false;
	private Thread scrollThread;
	private Semaphore acquirer = new Semaphore(0);
	private boolean acknowledged = true;
	
	public ColorPanel(FractalPanel fp){
		fractalPanel = fp;
		setLayout(new BorderLayout());
		
		labelPanel.setLayout(new GridLayout(4,1));
		labelPanel.add(redLabel);
		labelPanel.add(greenLabel);
		labelPanel.add(blueLabel);
		labelPanel.add(gradientLabel);
		add(labelPanel, BorderLayout.WEST);
		
		int max = fp.getMaxOffset();//+ 1 to compensate for width of slider
		scrollPanel.setLayout(new GridLayout(4,1));
		redBar = new JScrollBar(JScrollBar.HORIZONTAL,
				fp.getRedOffset(), 1, 0, max);
		greenBar = new JScrollBar(JScrollBar.HORIZONTAL,
				fp.getGreenOffset(), 1, 0, max);
		blueBar = new JScrollBar(JScrollBar.HORIZONTAL,
				fp.getBlueOffset(), 1, 0, max);
		gradientBar = new JScrollBar(JScrollBar.HORIZONTAL,
				4, 1, 1, fp.getGradient() + 1);
		redBar.addAdjustmentListener(this);
		greenBar.addAdjustmentListener(this);
		blueBar.addAdjustmentListener(this);
		gradientBar.addAdjustmentListener(this);
		scrollPanel.add(redBar);
		scrollPanel.add(greenBar);
		scrollPanel.add(blueBar);
		scrollPanel.add(gradientBar);
		add(scrollPanel, BorderLayout.CENTER);
		
		scrollThread = new Thread(this);
		scrollThread.start();
	}
	
	public void adjustmentValueChanged(AdjustmentEvent e){
		//adjust = true;
		lastAdjustTime = System.currentTimeMillis();
		if(acknowledged){
			acknowledged = false;
			acquirer.release();
		}
	}
	
	public void run(){
		while(true){
			try{
				acquirer.acquire();//block until user moves a scrollBar
				//if(adjust){
					//long currentTime = System.currentTimeMillis();
					while(System.currentTimeMillis() - lastAdjustTime >= 1000){
						Thread.sleep(50);
					}
						//here we can adjust after 1000 milliseconds
						//adjust = false;
						fractalPanel.setRedOffset(redBar.getValue());
						fractalPanel.setGreenOffset(greenBar.getValue());
						fractalPanel.setBlueOffset(blueBar.getValue());
						fractalPanel.setGradient(gradientBar.getValue());
						fractalPanel.drawFractal();
						acknowledged = true;
				//}
			}//end try
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}	
}

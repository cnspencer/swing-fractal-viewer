//Charles Spencer
//FractalView
//a GUI program to show a Fractal and zoom in on it


//package main_package;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;

public class FractalViewer extends JFrame implements KeyListener{
	//holds the fractal
	private FractalPanel fractalPanel;
	
	//this is for going back to previous zoom
	//saves rectangles in pixel form
	//saves as {x, y, width, height}
	private LinkedList<Double[]> backList = new LinkedList<Double[]>();
	private LinkedList<Double[]> forwardList = new LinkedList<Double[]>();
	
	//GUI components
	private JPanel buttonPanel = new JPanel();
	private JButton zoomButton = new JButton("Zoom");
	private JButton backButton = new JButton("Back");
	private JButton forwardButton = new JButton("Forward");
	private JButton restartButton = new JButton("Restart");
	private JButton saveButton = new JButton("Save");
	private JButton preciseButton = new JButton("Precise");
	
	private ColorPanel colorPanel;
	
	//for saving images
	private JFileChooser fileChooser = new JFileChooser();
	private PNGFilter pngFilter = new PNGFilter();
	
	
	public FractalViewer(FractalPanel.FractalType ft, double juliaX, double juliaY){
		super("Fractal Viewer");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.addKeyListener(this);
		this.setLayout(new BorderLayout());
		
		if (ft == FractalPanel.FractalType.MANDELBROT_SET) {
			fractalPanel = new FractalPanel(400, 400, this);
		}
		else {
			System.out.println(juliaX);
			System.out.println(juliaY);
			fractalPanel = new FractalPanel(400, 400, this, juliaX, juliaY);
		}
		add(fractalPanel, BorderLayout.CENTER);

		add(buttonPanel, BorderLayout.EAST);
		buttonPanel.setLayout(new GridLayout(10, 1));
		
		//--zoom button--
		buttonPanel.add(zoomButton);
		zoomButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(fractalPanel.getSelection() == null) return;				
				Rectangle selection = fractalPanel.getSelection();
								
				double pixelsPerUnit = fractalPanel.getBufferedImage().getWidth()
					/fractalPanel.getUnitWidth();
				
				Double[] backFrame = new Double[4];
				backFrame[0] = fractalPanel.getUnitPositionX();
				backFrame[1] = fractalPanel.getUnitPositionY();
				backFrame[2] = fractalPanel.getUnitWidth();
				backFrame[3] = fractalPanel.getUnitHeight();
				backList.push(backFrame);
				
				forwardList.clear();
				
				fractalPanel.setUnitPositionX(fractalPanel.getUnitPositionX() + 
						selection.getMinX()/pixelsPerUnit);
				fractalPanel.setUnitPositionY(fractalPanel.getUnitPositionY() - 
						selection.getMinY()/pixelsPerUnit);
				fractalPanel.setUnitWidth(selection.getWidth()
						/pixelsPerUnit);
				fractalPanel.setUnitHeight(selection.getHeight()
						/pixelsPerUnit);
				
				fractalPanel.drawFractal();
				fractalPanel.setSelection(null);				
				//fractalPanel.repaint();
			}
		});
		
		//--back button--
		buttonPanel.add(backButton);
		backButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(backList.size() < 1) return;
				
				Double[] forwardFrame = new Double[4];
				forwardFrame[0] = fractalPanel.getUnitPositionX();
				forwardFrame[1] = fractalPanel.getUnitPositionY();
				forwardFrame[2] = fractalPanel.getUnitWidth();
				forwardFrame[3] = fractalPanel.getUnitHeight();
				forwardList.push(forwardFrame);
				
				Double[] rect = backList.pop();
				fractalPanel.setUnitPositionX(rect[0]);
				fractalPanel.setUnitPositionY(rect[1]);
				fractalPanel.setUnitWidth(rect[2]);
				fractalPanel.setUnitHeight(rect[3]);
				
				fractalPanel.drawFractal();
				fractalPanel.setSelection(null);	
			}
		});
		
		//--forward button--
		buttonPanel.add(forwardButton);
		forwardButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(forwardList.size() < 1) return;
				
				Double[] backFrame = new Double[4];
				backFrame[0] = fractalPanel.getUnitPositionX();
				backFrame[1] = fractalPanel.getUnitPositionY();
				backFrame[2] = fractalPanel.getUnitWidth();
				backFrame[3] = fractalPanel.getUnitHeight();
				backList.push(backFrame);

				Double[] rect = forwardList.pop();
				fractalPanel.setUnitPositionX(rect[0]);
				fractalPanel.setUnitPositionY(rect[1]);
				fractalPanel.setUnitWidth(rect[2]);
				fractalPanel.setUnitHeight(rect[3]);
				
				fractalPanel.drawFractal();
				fractalPanel.setSelection(null);	
			}
		});
		
		//--restart button--
		buttonPanel.add(restartButton);
		restartButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Double[] backFrame = new Double[4];
				backFrame[0] = fractalPanel.getUnitPositionX();
				backFrame[1] = fractalPanel.getUnitPositionY();
				backFrame[2] = fractalPanel.getUnitWidth();
				backFrame[3] = fractalPanel.getUnitHeight();
				backList.push(backFrame);

				Dimension dimension = fractalPanel.getSize();
				
				fractalPanel.setUnitPositionX(FractalPanel.DEFAULT_UNIT_POSITION_X);
				fractalPanel.setUnitPositionY(FractalPanel.DEFAULT_UNIT_POSITION_Y);
				fractalPanel.setUnitWidth(dimension.getWidth() / FractalPanel.PIXELS_PER_UNIT);
				fractalPanel.setUnitHeight(dimension.getHeight() / FractalPanel.PIXELS_PER_UNIT);
				
				fractalPanel.drawFractal();
				fractalPanel.setSelection(null);				
				fractalPanel.repaint();				
			}
		});
		
		//--save button--
		buttonPanel.add(saveButton);
		//setup file path
		fileChooser.setFileFilter(pngFilter);
		saveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){				
				//here we call the file chooser
				int returnVal = fileChooser.showSaveDialog(FractalViewer.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					try{
						File saveFile = fileChooser.getSelectedFile();
						if(!saveFile.getName().endsWith(".png"))						
							saveFile = new File(
									saveFile.getAbsolutePath() +
									".png");
						
						ImageIO.write(fractalPanel.getBufferedImage(),
							"png", saveFile);
					}
					catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}
		});
		
		//--precise button--
		buttonPanel.add(preciseButton);
		preciseButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				fractalPanel.setBePrecise(fractalPanel.getBePrecise() ? false : true);
				fractalPanel.drawFractal();
				fractalPanel.setSelection(null);	
			}
		});
		
		colorPanel = new ColorPanel(fractalPanel);
		add(colorPanel, BorderLayout.SOUTH);
		
		fractalPanel.setPreferredSize(new Dimension(fractalPanel.getBufferedImage().getWidth(),
				fractalPanel.getBufferedImage().getHeight()));		
		pack();
		setVisible(true);
		
		
	}//end constructor
	
	//---------KeyListener methods------------
	
	public void keyPressed(KeyEvent ke){}
	
	public void keyReleased(KeyEvent ke){
		switch(ke.getKeyCode()){
		case KeyEvent.VK_ESCAPE:
			//running = false;
			//may need to wait on colorPanel to finish
			this.setVisible(false);
			this.dispose();
			System.exit(0);
		}
	}
	
	public void keyTyped(KeyEvent ke){}
	
	//----------
	
	//------------MAIN------------------
	
	public static void main(String[] args){		
		new FractalViewer(FractalPanel.FractalType.MANDELBROT_SET, 0.0, 0.0);
	}
	
	//-----FileFilter for saving the image in .png format---------
	
	private class PNGFilter extends javax.swing.filechooser.FileFilter{
		public boolean accept(File file){
			return file.getName().endsWith(".png");
		}
		public String getDescription(){
			return "PNG Images";
		}
	}

}

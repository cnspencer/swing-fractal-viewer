//package main_package;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.concurrent.Semaphore;
import java.math.BigDecimal;
import java.math.MathContext;

public class FractalPanel extends JPanel implements 
		MouseListener, MouseMotionListener, Runnable{
	
	private BufferedImage bufImage;
	private Dimension size;
	
	private Rectangle selection = null;
	private double startX = 0;
	private double startY = 0;
	
	private Color rectColor = Color.green;
	
	public static double DEFAULT_UNIT_POSITION_X = -5;
	private double unitPositionX = DEFAULT_UNIT_POSITION_X;

	public static double DEFAULT_UNIT_POSITION_Y = 5;
	private double unitPositionY = DEFAULT_UNIT_POSITION_Y;
	
	private double unitWidth;
	private double unitHeight;
	
	public static double PIXELS_PER_UNIT = 40;
	
	//for adjusting colors
	private int maxOffset = 70;
	private int redOffset = 50;
	private int greenOffset = 60;
	private int blueOffset = 0;
	private int gradient = 4;
	
	private FractalViewer fract;
	
	private Semaphore redrawSem = new Semaphore(1);
	
	public enum FractalType
	{
		JULIA_SET,
		MANDELBROT_SET
	}
	
	private FractalType fractalType;
	
	private double juliaSetReal;
	
	private double juliaSetImaginary;
	
	private boolean bBePrecise = false;
	
	public FractalPanel(int width, int height, FractalViewer fv){
		fract = fv;
		fractalType = FractalType.MANDELBROT_SET;
		
		unitWidth = width / PIXELS_PER_UNIT;
		unitHeight = height / PIXELS_PER_UNIT;
		
		size = new Dimension(width, height);
		setSize(size);
		bufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		
		//new Thread(this).start();//to draw fractal
		addMouseListener(this);
		addMouseMotionListener(this);
		
		//start the draw thread
		new Thread(this).start();
	}
	
	public FractalPanel(int width, int height, FractalViewer fv, double juliaSetReal, double juliaSetImaginary){
		fract = fv;
		fractalType = FractalType.JULIA_SET;
		this.juliaSetReal = juliaSetReal;
		this.juliaSetImaginary = juliaSetImaginary;
		
		unitWidth = width / PIXELS_PER_UNIT;
		unitHeight = height / PIXELS_PER_UNIT;
		
		size = new Dimension(width, height);
		setSize(size);
		bufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		
		//new Thread(this).start();//to draw fractal
		addMouseListener(this);
		addMouseMotionListener(this);
		
		//start the draw thread
		new Thread(this).start();
	}
	
	
	public void paint(Graphics g){		
		g.drawImage(bufImage, 0, 0, null);
		
		if(selection != null){
			g.setColor(rectColor);
			g.drawRect(selection.x, selection.y, selection.width, selection.height);
		}
		
		g.dispose();
	}
	
	public void drawFractal(){
		//spin off a thread to do this
		redrawSem.release();
	}
	
	public void run(){
		mainLoop:
		while(true){
			//first acquire the semaphore
			redrawSem.acquireUninterruptibly();
			redrawSem.drainPermits();
			
//			for(int i = 0; i < bufImage.getWidth(); i++){
//				//check in the middle of drawing
//				if(redrawSem.availablePermits() > 0){
//					continue mainLoop;
//				}
//				
//				for(int j = 0; j < bufImage.getHeight(); j++){
//					bufImage.setRGB(i, j, getColorMandelbrot(i, j, 5000));
//				}
//			}
//			repaint();
			for(int j = 0; j < bufImage.getHeight(); j++){
				//check in the middle of drawing
				if(redrawSem.availablePermits() > 0){
					continue mainLoop;
				}

				for(int i = 0; i < bufImage.getWidth(); i++){
					if (fractalType == FractalType.MANDELBROT_SET) {
						if (bBePrecise) {
							bufImage.setRGB(i, j, getColorMandelbrotPrecise(i, j, 10000));
						}
						else {
							bufImage.setRGB(i, j, getColorMandelbrot(i, j, 10000));
						}
					}
					else {
						bufImage.setRGB(i, j, getColorJulia(i, j, 10000));
					}
				}
				repaint();
			}
		}
	}
	
	//I stole this from Wikipedia
	public int getColorMandelbrot(int pixelX, int pixelY, int count){
		double pixelsPerUnit = bufImage.getWidth()/unitWidth;
		double pixelsPerUnitY = bufImage.getHeight()/unitHeight;
		
		double x0 = unitPositionX + (pixelX/pixelsPerUnit);
		double x = x0;
		double y0 = unitPositionY - (pixelY/pixelsPerUnitY);
		double y = y0;
		
//		double directionX = 0;
//		double directionY = 0;
		
		int iteration = 0;
		int max_iteration = count;
		  
//		while ( x*x + y*y <= 4  &&  iteration < max_iteration ) {
//			double xtemp = x*x - y*y + x0; // the real part
//			y = 2*x*y + y0; // the imaginary part
//
//			x = xtemp;
//
//			iteration = iteration + 1;
//		}
		
		while ( x*x + y*y <= 4  &&  iteration < max_iteration ) {
			double xtemp = x*x - y*y + x0; // the real part
			y = 2*x*y + y0; // the imaginary part

			x = xtemp;
			
//			if (iteration == 0) {
//				directionX = x - x0;
//				directionY = y - y0;
//			}

			iteration = iteration + 1;
		}
		 
		if ( iteration == max_iteration ) 
			return 0x00000000;
		else {
			//beautiful color scheme:
			//red part
			int color = (int)(((Math.sin(//causes colors to cycle through a pattern
					Math.sqrt(iteration)//makes cycling more and more gradual(otherwise looks like fuzz in some places)
					/((double)gradient) + (((double)(2*redOffset))/maxOffset
					)*Math.PI))+1.0)*127)  <<16;
			//green
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*greenOffset))/maxOffset
					)*Math.PI))+1.0)*127) << 8;
			//blue
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*blueOffset))/maxOffset
					)*Math.PI))+1.0)*127);
			return color;
			
//			int color;
//			
//			directionX = x - x0;
//			directionY = y - y0;
//			
//			if (directionX > 0) {
//				if (directionY > 0) {
//					color = 0x000000FF;
//				}
//				else {
//					color = 0x00FF0000;
//				}
//			}
//			else {
//				if (directionY > 0) {
//					color = 0x0000FF00;
//				}
//				else {
//					color = 0x00FFFF00;
//				}
//			}
//			return color;
		}
	}
	
	public int getColorMandelbrotPrecise(int pixelXSmall, int pixelYSmall, int count){
		MathContext mc = MathContext.DECIMAL128;
		BigDecimal pixelX = new BigDecimal(pixelXSmall, mc);
		BigDecimal pixelY = new BigDecimal(pixelYSmall, mc);
		BigDecimal pixelsPerUnit = new BigDecimal(bufImage.getWidth()/unitWidth, mc);
		BigDecimal pixelsPerUnitY = new BigDecimal(bufImage.getHeight()/unitHeight, mc);
		BigDecimal unitPositionXBig = new BigDecimal(unitPositionX, mc);
		BigDecimal unitPositionYBig = new BigDecimal(unitPositionY, mc);
		
		BigDecimal x0 = unitPositionXBig.add(pixelX.divide(pixelsPerUnit, mc), mc);
		BigDecimal x = new BigDecimal(x0.doubleValue());
		BigDecimal y0 = unitPositionYBig.subtract(pixelY.divide(pixelsPerUnitY, mc), mc);
		BigDecimal y = new BigDecimal(y0.doubleValue());
		
		int iteration = 0;
		int max_iteration = count;
		
		BigDecimal two = new BigDecimal(2);
		
		while (x.multiply(x, mc).add(y.multiply(y, mc), mc).doubleValue() <= 4.0  &&  iteration < max_iteration) {
			BigDecimal xtemp = x.multiply(x, mc).subtract(y.multiply(y, mc), mc).add(x0, mc); // the real part
			y = x.multiply(y, mc).multiply(two, mc).add(y0, mc); // the imaginary part

			x = xtemp;

			iteration = iteration + 1;
		}
		 
		if ( iteration == max_iteration ) 
			return 0x00000000;
		else {
			//beautiful color scheme:
			//red part
			int color = (int)(((Math.sin(//causes colors to cycle through a pattern
					Math.sqrt(iteration)//makes cycling more and more gradual(otherwise looks like fuzz in some places)
					/((double)gradient) + (((double)(2*redOffset))/maxOffset
					)*Math.PI))+1.0)*127)  <<16;
			//green
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*greenOffset))/maxOffset
					)*Math.PI))+1.0)*127) << 8;
			//blue
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*blueOffset))/maxOffset
					)*Math.PI))+1.0)*127);
			return color;
		}

//		if ((pixelXSmall % 8) >= 4) {
////		if ((pixelXSmall % 2) >= 1) {
//			if ((pixelYSmall % 8) >= 4) {
////			if ((pixelYSmall % 2) >= 1) {
//				return 0x00FFFF00;
//			}
//			else {
//				return 0x0000FFFF;
//			}
//		}
//		else
//		{
//			if ((pixelYSmall % 8) >= 4) {
////			if ((pixelYSmall % 2) >= 1) {
//				return 0x0000FFFF;
//			}
//			else {
//				return 0x00FFFF00;
//			}
//		}
	}
	
	public int getColorJulia(int pixelX, int pixelY, int count){
		double pixelsPerUnit = bufImage.getWidth()/unitWidth;
		double pixelsPerUnitY = bufImage.getHeight()/unitHeight;
		
		double x0 = unitPositionX + (pixelX/pixelsPerUnit);
		double x = x0;
		double y0 = unitPositionY - (pixelY/pixelsPerUnitY);
		double y = y0;
		
		int iteration = 0;
		int max_iteration = count;
		  
		while ( x*x + y*y <= 4  &&  iteration < max_iteration ) {
			double xtemp = x*x - y*y + juliaSetReal; // the real part
			y = 2*x*y + juliaSetImaginary; // the imaginary part

			x = xtemp;

			iteration = iteration + 1;
		}
		 
		if ( iteration == max_iteration ) 
			return 0x00000000;
		else {
			//beautiful color scheme:
			//red part
			int color = (int)(((Math.sin(//causes colors to cycle through a pattern
					Math.sqrt(iteration)//makes cycling more and more gradual(otherwise looks like fuzz in some places)
					/((double)gradient) + (((double)(2*redOffset))/maxOffset
					)*Math.PI))+1.0)*127)  <<16;
			//green
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*greenOffset))/maxOffset
					)*Math.PI))+1.0)*127) << 8;
			//blue
			color |= (int)(((Math.sin(
					Math.sqrt(iteration)
					/((double)gradient) + (((double)(2*blueOffset))/maxOffset
					)*Math.PI))+1.0)*127);
			return color;
		}
	}
	
	//---------MouseListener and MouseMotionListener methods-----------------------

	public void mousePressed(MouseEvent e){
		if (e.isPopupTrigger()) {
			new FractalPopupMenu(this, e.getX(), e.getY()).show(e.getComponent(), e.getX(), e.getY());
		}
		else {
			startX = e.getX();
			startY = e.getY();
			selection = new Rectangle((int)startX, (int)startY, 0, 0);
			repaint();
		}
	}
	public void mouseReleased(MouseEvent e){
		if (e.isPopupTrigger()) {
			new FractalPopupMenu(this, (e.getX()/PIXELS_PER_UNIT) - unitWidth / 2, -(e.getY()/PIXELS_PER_UNIT - unitHeight / 2)).show(e.getComponent(),
					e.getX(), e.getY());
		}
		else {
			repaint();
		}
	}
	public void mouseDragged(MouseEvent e){

		if(selection != null){
			double panelProportion = unitWidth / unitHeight;
			
			double x = startX < e.getX() ? startX : e.getX();
			double y = startY < e.getY() ? startY : e.getY();
			double width = Math.abs(e.getX() - startX);
			double height = Math.abs(e.getY() - startY);
			
			double dragProportion = width / height;
			
			double newWidth;
			double newHeight;
			
			if (panelProportion > dragProportion)
			{
				newWidth = panelProportion * height;
				newHeight = height;
			}
			else
			{
				newWidth = width;
				newHeight = width / panelProportion;
			}
			
			
			
			selection.setRect(x, y, newWidth, newHeight);
			
			fract.repaint();
		}
	}
	
	public void mouseClicked(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mouseMoved(MouseEvent e){}
	
	//-----------Set and Get-------------
	public Rectangle getSelection(){return selection;}
	public double getUnitPositionX(){return unitPositionX;}
	public double getUnitPositionY(){return unitPositionY;}
	public double getUnitWidth(){return unitWidth;}
	public double getUnitHeight(){return unitHeight;}
	public int getMaxOffset(){return maxOffset;}
	public int getRedOffset(){return redOffset;}
	public int getGreenOffset(){return greenOffset;}
	public int getBlueOffset(){return blueOffset;}
	public int getGradient(){return gradient;}
	public BufferedImage getBufferedImage(){return bufImage;}
	public FractalType getFractalType(){return fractalType;}
	public boolean getBePrecise(){return bBePrecise;}
	
	public void setSelection(Rectangle s){selection = s;}
	public void setUnitPositionX(double num){unitPositionX = num;}
	public void setUnitPositionY(double num){unitPositionY = num;}
	public void setUnitWidth(double num){unitWidth = num;}
	public void setUnitHeight(double num){unitHeight = num;}
	public void setMaxOffset(int num){maxOffset = num;}
	public void setRedOffset(int num){redOffset = num;}
	public void setGreenOffset(int num){greenOffset = num;}
	public void setBlueOffset(int num){blueOffset = num;}
	public void setGradient(int num){gradient = num;}
	public void setFractalType(FractalType fType){fractalType = fType;}
	public void setBePrecise(boolean b){bBePrecise = b;}

}
